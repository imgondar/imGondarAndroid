# walkAndroid
徒步大会Android客户端

###JAVA代码
1. **结构 MVP** ， **网络请求 Retrofit+RxJava**

    参考:

    [Android 教你一步步搭建MVP+Retrofit+RxJava网络请求框架](http://www.jianshu.com/p/7b839b7c5884)

    [【Android】RxJava + Retrofit完成网络请求](http://www.jianshu.com/p/1fb294ec7e3b)

    [给 Android 开发者的 RxJava 详解](http://gank.io/post/560e15be2dca930e00da1083#toc_1)

    Retrofit拦截器，参数拦截并加签，参考：

    [Android网络请求使用Retrofit+OkHttp，如何获取请求参数 ？](http://blog.csdn.net/u010897392/article/details/53761917)


2. **ButterKnife**  注解

    [ButterKnife](https://github.com/JakeWharton/butterknife)

    ```
    // Activity 中，要在setContentView后调用
    ButterKnife.bind(this);
    // Fragment 中，要在OnViewCreate中调用
    ButterKnife.bind(this, view);
    // 效果等同findViewById
    @BindView(R.id.imageview)
    ImageView imageView;
    ```

### App交互效果
1. **RefreshLayout**    下拉刷新上拉加载
    ```
    refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            refreshLayout.setRefreshing(false);
        }
    });

    refreshLayout.setOnLoadListener(new RefreshLayout.OnLoadListener() {
        @Override
        public void onLoad() {
            refreshLayout.setLoading(false);
        }
    });
    ```

2. **ImmersionBar**     沉浸式布局

    [ImmersionBar](https://github.com/gyf-dev/ImmersionBar)
    集成ImmersionBar实现沉浸式布局


3. **ProgressLoadingView**   加载圈
    ```
    /**
     * 显示加载圈
     */
    public void showLoading() {
        if (loading == null) {
            loading = new ProgressLoadingView(this, R.style.CustomDialog);
        }
        loading.show();
    }

    /**
     * 隐藏加载圈
     */
    public void dismissLoading() {
        if (loading != null) {
            loading.dismiss();
        }
    }
    ```

4. **SwipeBackActivity**  侧滑返回

    只需要继承`SwipeBackActivity`就可以


5. **Tablayout**  切换控件，配合viewpage+fragment

    ```
    // 设置标题和fragment
    titleList.add("tab1");
    titleList.add("tab2");
    viewList.add(new PlayFragment());
    viewList.add(new MyFragment());
    // 设置适配器
    MyPagerAdapter adapter = new MyPagerAdapter(getFragmentManager());
    viewPager.setAdapter(adapter);
    tabLayout.setupWithViewPager(viewPager);//将TabLayout和ViewPager关联起来。
    tabLayout.setTabsFromPagerAdapter(adapter);//给Tabs设置适配器
    ```
    ```
    //ViewPager适配器
    class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return viewList.size();//页卡数
        }

        @Override
        public Fragment getItem(int position) {
            return viewList.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);//页卡标题
        }
    }
    ```


6. **PhotoView**  查看大图，支持缩放的view

    [PhotoView](https://github.com/chrisbanes/PhotoView)


### 工具类
1. **AppVersionUtil**  获取版本
2. **BitmapUtil**  图片处理（裁剪，缩放，压缩等）
3. **CheckNetUtil**  检查网络
4. **CollectionUtil**  集合判断
5. **DensityUtil**  尺寸转换（dp，sp，px）
6. **DialogCreator**  Dialog
7. **FileUtils**  文件处理（复制，删除等）
8. **KeyBoardUtils**  软键盘
9. **Logger**  打印Log
10. **MD5Util**  MD5加密
11. **NotificationHelper**  通知栏
12. **ScreenUtil**  屏幕宽高工具类
13. **SDCardUtil**  SD卡相关
14. **SharePreferenceManager**  sp工具类
15. **StringUtil**  字符串（字符串连接判空等）
16. **TimeUtil**  时间工具类
17. **ToastUtil**  吐司
18. **UpdateApkManagerUtil**  检查更新


