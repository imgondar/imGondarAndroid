package com.imgondarandroid.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.imgondarandroid.R;
import com.imgondarandroid.activity.MainActivity;


/**
 * Created by wangmuxiong on 2017/11/30.
 */

public class NotificationHelper {

    @SuppressWarnings("deprecation")
    public static void showMessageNotification(Context context,
                                               NotificationManager nm, String title, String msgContent, boolean voice, boolean vibration) {

        PendingIntent contentIntent = null;
        Intent select = new Intent();
        // 处于后台
        if (Util.isBackground(context)) {
            select.setClass(context, MainActivity.class);
//            select.putExtra(Constants.FROM_JPUSH, true);
            select.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } else {
            select.setClass(context, context.getClass());
        }

        contentIntent = PendingIntent.getActivity(context, 0,
                select, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(msgContent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(contentIntent)
                .build();
        showMessageNotificationLocal(context, nm, notification, 1,
                voice, vibration);
    }

    /**
     * 展示通知栏
     * @param context
     * @param nm
     * @param notification
     * @param notificationId
     * @param needSound
     * @param needVibrate
     */
    private static void showMessageNotificationLocal(Context context,
                                                     NotificationManager nm, Notification notification,
                                                     int notificationId, boolean needSound, boolean needVibrate) {
        notification.ledARGB = 0xff00ff00;
        notification.ledOnMS = 300;
        notification.ledOffMS = 1000;
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        if (needSound && needVibrate) {
            notification.defaults = Notification.DEFAULT_SOUND
                    | Notification.DEFAULT_VIBRATE;
        } else if (needSound) {
            notification.defaults = Notification.DEFAULT_SOUND;
        } else if (needVibrate) {
            notification.defaults = Notification.DEFAULT_VIBRATE;
        } else if (!needSound) {
            notification.sound = null;
        } else if (!needVibrate) {
            notification.vibrate = null;
        }

        nm.notify(notificationId, notification);
    }
}
