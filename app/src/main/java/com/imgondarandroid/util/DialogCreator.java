package com.imgondarandroid.util;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imgondarandroid.R;


public class DialogCreator {
    static Dialog showHintDialog;

    /**
     * 自定义弹框
     */
    public static Dialog showHintDialog(Context context, String title, String message, View.OnClickListener clickListener) {
        AlertDialog.Builder Builder = new AlertDialog.Builder(context);
        //    通过LayoutInflater来加载一个xml的布局文件作为一个View对象
        View view = LayoutInflater.from(context).inflate(R.layout.app_dialog, null);

        TextView titleTv = (TextView) view.findViewById(R.id.title);
        TextView messageTv = (TextView) view.findViewById(R.id.message);
        TextView submit = (TextView) view.findViewById(R.id.submit);
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        titleTv.setText(title);
        messageTv.setText(message);
        submit.setOnClickListener(clickListener);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHintDialog.dismiss();
            }
        });
        //  设置我们自己定义的布局文件作为弹出框的Content
        Builder.setView(view);
        showHintDialog = Builder.show();
        //此处设置位置窗体大小
        showHintDialog.getWindow().setLayout(DensityUtil.dp2px(context, 300), LinearLayout.LayoutParams.WRAP_CONTENT);
        return showHintDialog;
    }

}
