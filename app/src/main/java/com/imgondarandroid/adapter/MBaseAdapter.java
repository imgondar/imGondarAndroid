package com.imgondarandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.imgondarandroid.R;
import com.imgondarandroid.view.ProgressLoadingView;

import java.util.List;

/**
 * Created by wangmuxiong on 2017/9/18.
 */

public class MBaseAdapter<T> extends BaseAdapter{

    protected Context context;
    protected List<T> list;
    protected LayoutInflater inflater;
    private ProgressLoadingView loading; //加载圈

    public MBaseAdapter(){}
    public MBaseAdapter(Context context, List<T> list){
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (null == list) return 0;
        return list.size() == 0 ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return list != null ?list.get(i):i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }

    /**
     * 显示加载圈
     */
    public void showLoading() {
        if (loading == null) {
            loading = new ProgressLoadingView(context, R.style.CustomDialog);
        }
        loading.show();
    }

    /**
     * 隐藏加载圈
     */
    public void dismissLoading() {
        if (loading != null) {
            loading.dismiss();
        }
    }
}
