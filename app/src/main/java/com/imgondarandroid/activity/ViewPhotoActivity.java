package com.imgondarandroid.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.chrisbanes.photoview.OnViewTapListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.imgondarandroid.Constants;
import com.imgondarandroid.R;
import com.imgondarandroid.service.presenter.SavePicturePresenter;
import com.imgondarandroid.service.view.SavePictureView;
import com.imgondarandroid.util.DialogCreator;
import com.imgondarandroid.util.swipeback.app.SwipeBackActivity;
import com.imgondarandroid.view.HackyViewPager;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;

/**
 * Created by wangmuxiong on 2017/12/13.
 */

public class ViewPhotoActivity extends SwipeBackActivity {

    @BindView(R.id.view_pager)
    HackyViewPager viewPager;
    @BindView(R.id.tvDqimgCnt)
    TextView tvDqimgCnt;
    @BindView(R.id.tvImgCnt)
    TextView tvImgCnt;

    private ArrayList<String> mDatas = new ArrayList<>();
    private int position;
    private int imgCnt;

    private SavePicturePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);

        initData();

        presenter = new SavePicturePresenter(this);
        presenter.onCreate();
        presenter.attachView(view);
    }

    @Override
    protected boolean isImmersionBarEnabled() {
        return false;
    }

    private void initData() {
        //点击图片的位置
        position = getIntent().getIntExtra("position", 0);
        //获取传递过来的图片地址
        mDatas = getIntent().getStringArrayListExtra(Constants.MIMAGES);
        //图片个数
        imgCnt = getIntent().getIntExtra(Constants.IMG_COUNT, 0);
        tvDqimgCnt.setText((viewPager.getCurrentItem() + 1) + "/");
        tvImgCnt.setText(imgCnt + "");
        ImageAdapter imageAdapter = new ImageAdapter();
        viewPager.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();
        viewPager.setCurrentItem(position, false);
        tvDqimgCnt.setText(position + 1 + "/");
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                String num = (viewPager.getCurrentItem() + 1) + "";
                tvDqimgCnt.setText(num + "/");
            }
        });
    }

    public class ImageAdapter extends PagerAdapter {

        private String imgurl;

        @Override
        public int getCount() {
            return mDatas.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            //创建显示图片的控件
            PhotoView photoView = new PhotoView(container.getContext());
            //设置背景颜色
            photoView.setBackgroundColor(Color.BLACK);
            //把photoView添加到viewpager中，并设置布局参数
            container.addView(photoView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //使用使用Glide进行加载图片进行加载图片
            //加载当前PhtotoView要显示的数据
            imgurl = mDatas.get(position);
            Glide.with(ViewPhotoActivity.this)
                    .load(imgurl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(photoView);
            //图片单击事件的处理
            photoView.setOnViewTapListener(new OnViewTapListener() {
                @Override
                public void onViewTap(View view, float x, float y) {
                    finish();
                }
            });
            savePicture(photoView, imgurl);
            return photoView;
        }
    }

    private Dialog dialog;
    private void savePicture(PhotoView view, final String url) {
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                dialog = DialogCreator.showHintDialog(ViewPhotoActivity.this, "提示", "保存图片到手机", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // 判断是否有SD卡
                        if (Environment.getExternalStorageState().equals(
                                Environment.MEDIA_MOUNTED)) {
                            String path = getExternalCacheDir()
                                    + "/imgondar/" + new Date().getTime() + "downImg.png";
                            presenter.savePic(url, path);
                            dialog.dismiss();
                        } else {
                            getToast("请先插入SD卡");
                        }
                    }
                });
                return false;
            }
        });
    }

    SavePictureView view = new SavePictureView() {
        @Override
        public void onSuccess(String path) {

        }

        @Override
        public void onError(String result) {
            getToast(result);
        }
    };
}
