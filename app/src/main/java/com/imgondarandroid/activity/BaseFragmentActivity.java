package com.imgondarandroid.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.gyf.barlibrary.ImmersionBar;
import com.imgondarandroid.R;
import com.imgondarandroid.view.ProgressLoadingView;

import butterknife.ButterKnife;

/**
 * Created by wangmuxiong on 2017/9/14.
 */

public class BaseFragmentActivity extends FragmentActivity{

    private ProgressLoadingView loading; //加载圈
    protected ImmersionBar mImmersionBar; // 沉浸式状态栏

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mImmersionBar = ImmersionBar.with(this);   //所有子类都将继承这些相同的属性
        mImmersionBar.titleBar(R.id.titlebar)
                .keyboardEnable(true)
                .init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mImmersionBar != null)
            mImmersionBar.destroy();  //必须调用该方法，防止内存泄漏，不调用该方法，如果界面bar发生改变，在不关闭app的情况下，退出此界面再进入将记忆最后一次bar改变的状态
    }

    /**
     * 设置标题
     * @param text
     */
    public void setTitle(String text) {
        TextView title = (TextView) findViewById(R.id.title_tv);
        title.setText(text);
    }

    /**
     * 设置标题
     * @param text
     */
    public void setTitle(int text) {
        TextView title = (TextView) findViewById(R.id.title_tv);
        title.setText(text);
    }

    /**
     * 设置标题栏操作按钮隐藏
     * @param gone
     */
    public void goneTitleAction(boolean gone){
        if (gone) {
            findViewById(R.id.right_action).setVisibility(View.GONE);
        } else {
            findViewById(R.id.right_action).setVisibility(View.VISIBLE);
        }
    }

    /**
     * 吐司
     * @param text
     */
    public void getToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    /**
     * 吐司
     * @param text
     */
    public void getToast(int text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    /**
     * 显示加载圈
     */
    public void showLoading() {
        if (loading == null) {
            loading = new ProgressLoadingView(this, R.style.CustomDialog);
        }
        loading.show();
    }

    /**
     * 隐藏加载圈
     */
    public void dismissLoading() {
        if (loading != null) {
            loading.dismiss();
        }
    }
}
