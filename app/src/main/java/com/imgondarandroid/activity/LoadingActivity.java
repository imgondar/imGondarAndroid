package com.imgondarandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.imgondarandroid.R;
import com.imgondarandroid.util.SharePreferenceManager;

/**
 * Created by wangmuxiong on 2017/9/15.
 * 启动页
 */

public class LoadingActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_loading);
    }

    @Override
    protected void onStart() {
        super.onStart();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2500);
                    Intent intent;
                    // 是否登录
                    if (SharePreferenceManager.getIsLogin()) {
                        intent = new Intent(LoadingActivity.this, MainActivity.class);
                    } else {
                        intent = new Intent(LoadingActivity.this, MainActivity.class);
                    }
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
