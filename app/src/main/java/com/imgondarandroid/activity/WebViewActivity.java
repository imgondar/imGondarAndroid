package com.imgondarandroid.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.imgondarandroid.Constants;
import com.imgondarandroid.R;
import com.imgondarandroid.util.swipeback.app.SwipeBackActivity;

import butterknife.OnClick;

/**
 * Created by wangmuxiong on 2017/9/22.
 */

public class WebViewActivity extends SwipeBackActivity implements View.OnClickListener{

    private WebView webView;
    private String url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview);
        url = getIntent().getStringExtra(Constants.URL);
        if (TextUtils.isEmpty(url)) {
            finish();
            return;
        }
        initView();
    }

    private void initView() {
        webView = (WebView) findViewById(R.id.web_view);
        //声明WebSettings子类
        WebSettings webSettings = webView.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        webSettings.setJavaScriptEnabled(true);

        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小

        //缩放操作
        webSettings.setSupportZoom(true); //支持缩放，默认为true。是下面那个的前提。
        webSettings.setBuiltInZoomControls(true); //设置内置的缩放控件。若为false，则该WebView不可缩放
        webSettings.setDisplayZoomControls(false); //隐藏原生的缩放控件

        //其他细节操作
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); //关闭webview中缓存
        webSettings.setAllowFileAccess(true); //设置可以访问文件
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true); //支持通过JS打开新窗口
        webSettings.setLoadsImagesAutomatically(true); //支持自动加载图片
        webSettings.setDefaultTextEncodingName("utf-8");//设置编码格式
        Log.i(getLocalClassName(), url);
        webView.loadUrl(url);
        // 点击链接继续在当前browser中响应，而不是新开Android的系统browser中响应该链接
        webView.setWebViewClient(new MyWebViewClient());
//        // 设置setWebChromeClient对象
        webView.setWebChromeClient(wvcc);
    }

    @Override
    @OnClick({R.id.back_action})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_action:
                if (webView.canGoBack()) {
                    webView.goBack(); // goBack()表示返回WebView的上一页面
                    return;
                }
                finish();
                break;
        }
    }

    // 点击链接继续在当前browser中响应，而不是新开Android的系统browser中响应该链接，必须覆盖
    // webview的WebViewClient对象
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }
    }

    // WebChromeClient是辅助WebView处理Javascript的对话框，网站图标，网站title，加载进度等
    WebChromeClient wvcc = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String titleStr) {
            super.onReceivedTitle(view, titleStr);
            setTitle(titleStr);
        }
    };

    // 设置回退
    // 覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (webView.canGoBack()) {
                webView.goBack(); // goBack()表示返回WebView的上一页面
                return true;
            }
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
