package com.imgondarandroid.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.imgondarandroid.R;
import com.imgondarandroid.fragment.MyFragment;
import com.imgondarandroid.fragment.PlayFragment;
import com.imgondarandroid.fragment.RouteFragment;
import com.imgondarandroid.fragment.WalkFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseFragmentActivity implements View.OnClickListener {

    @BindView(R.id.walk_tv)
    TextView walk_tv;
    @BindView(R.id.route_tv)
    TextView route_tv;
    @BindView(R.id.play_tv)
    TextView play_tv;
    @BindView(R.id.my_tv)
    TextView my_tv;

    private Fragment mContent = new WalkFragment();
    private WalkFragment walk = new WalkFragment();
    private RouteFragment route;
    private PlayFragment play;
    private MyFragment my;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setTitle(R.string.walk);
        switchContent(walk);
    }

    @Override
    @OnClick({R.id.title_tv, R.id.walk_tv, R.id.route_tv, R.id.play_tv, R.id.my_tv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title_tv:
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    // 申请一个（或多个）权限，并提供用于回调返回的获取码（用户定义)
                    requestPermissions(new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
                new ShareAction(MainActivity.this)
                        .withText("徒步大会")
                        .withMedia(new UMImage(MainActivity.this, R.mipmap.ic_launcher))
                        .setDisplayList(SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.SINA)
                        .setCallback(new UMShareListener() {
                            @Override
                            public void onStart(SHARE_MEDIA share_media) {

                            }

                            @Override
                            public void onResult(SHARE_MEDIA share_media) {

                            }

                            @Override
                            public void onError(SHARE_MEDIA share_media, Throwable throwable) {

                            }

                            @Override
                            public void onCancel(SHARE_MEDIA share_media) {

                            }
                        })
                        .open();
                break;
            case R.id.walk_tv:
                if (walk == null) {
                    walk = new WalkFragment();
                }
                setTitle(R.string.walk);
                switchContent(walk);
                break;
            case R.id.route_tv:
                if (route == null) {
                    route = new RouteFragment();
                }
                setTitle(R.string.route);
                switchContent(route);
                break;
            case R.id.play_tv:
                if (play == null) {
                    play = new PlayFragment();
                }
                setTitle(R.string.play);
                switchContent(play);
                break;
            case R.id.my_tv:
                if (my == null) {
                    my = new MyFragment();
                }
                setTitle(R.string.my);
                switchContent(my);
                break;
        }
    }

    /**
     * 修改显示的内容 不会重新加载
     **/
    public void switchContent(Fragment to) {
        if (mContent != to) {
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();
            if (!to.isAdded()) { // 先判断是否被add过

                transaction.hide(mContent).add(R.id.fragment, to)
                        .commit(); // 隐藏当前的fragment，add下一个到Activity中
            } else {

                transaction.hide(mContent).show(to).commit(); // 隐藏当前的fragment，显示下一个
            }
            mContent = to;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);//完成回调
    }

    private long mExitTime;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                getToast(R.string.exit_hint);
                mExitTime = System.currentTimeMillis();
            } else {
                onBackPressed();
            }
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
