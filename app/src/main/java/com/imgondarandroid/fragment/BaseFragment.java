package com.imgondarandroid.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.View;

import com.imgondarandroid.R;
import com.imgondarandroid.view.ProgressLoadingView;

import butterknife.ButterKnife;


/**
 * Created by wangmuxiong on 2017/9/14.
 */

public class BaseFragment extends Fragment {

    protected float mDensity;
    protected int mDensityDpi;
    protected int mWidth;
    protected int mHeight;
    protected float mRatio;
    protected int mAvatarSize;
    private Context mContext;

    private ProgressLoadingView loading; //加载圈

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this.getActivity();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        mDensity = dm.density;
        mDensityDpi = dm.densityDpi;
        mWidth = dm.widthPixels;
        mHeight = dm.heightPixels;
        mRatio = Math.min((float) mWidth / 720, (float) mHeight / 1280);
        mAvatarSize = (int) (50 * mDensity);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    /**
     * 显示加载圈
     */
    public void showLoading() {
        if (loading == null) {
            loading = new ProgressLoadingView(mContext, R.style.CustomDialog);
        }
        loading.show();
    }

    /**
     * 隐藏加载圈
     */
    public void dismissLoading() {
        if (loading != null) {
            loading.dismiss();
        }
    }

}
