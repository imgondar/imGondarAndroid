package com.imgondarandroid.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.imgondarandroid.Constants;
import com.imgondarandroid.R;
import com.imgondarandroid.activity.ViewPhotoActivity;
import com.imgondarandroid.service.entity.UserInfo;
import com.imgondarandroid.service.presenter.UserPresenter;
import com.imgondarandroid.service.view.UserView;
import com.imgondarandroid.util.Logger;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by wangmuxiong on 2017/12/12.
 */

public class WalkFragment extends BaseFragment implements View.OnClickListener{

    @BindView(R.id.imageview)
    ImageView imageView;

    ArrayList<String> mDatas = new ArrayList<>();

    private UserPresenter userPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_walk, container, false);
        mDatas.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1513177520379&di=a733b7a0cbaebefad719f7adab973a1b&imgtype=0&src=http%3A%2F%2Fwww.qqzhi.com%2Fuploadpic%2F2015-01-06%2F011458495.jpg");
        mDatas.add("http://pic2116.ytqmx.com:82/2017/0711/37/4.jpg");

        userPresenter = new UserPresenter(getActivity());
        userPresenter.onCreate();
        userPresenter.attachView(userView);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Glide.with(this).load(mDatas.get(0)).crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
    }

    @Override
    @OnClick({R.id.imageview,R.id.login})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageview:
                Intent intent = new Intent(getActivity(), ViewPhotoActivity.class);
                intent.putExtra(Constants.MIMAGES, mDatas);
                intent.putExtra(Constants.IMG_COUNT, mDatas.size());
                startActivity(intent);
                break;
            case R.id.login:
//                login();
                userPresenter.login();
                break;
        }
    }

    private UserView userView = new UserView() {
        @Override
        public void onSuccess(UserInfo userInfo) {
            String s = new Gson().toJson(userInfo);
            Logger.i("WalkFragment", s);
        }

        @Override
        public void onError(String result) {
            Logger.i("WalkFragment", result);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        userPresenter.onStop();
    }
}
