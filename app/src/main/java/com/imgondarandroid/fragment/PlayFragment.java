package com.imgondarandroid.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imgondarandroid.Constants;
import com.imgondarandroid.R;
import com.imgondarandroid.activity.WebViewActivity;

import butterknife.OnClick;

/**
 * Created by wangmuxiong on 2017/12/12.
 */

public class PlayFragment extends BaseFragment implements View.OnClickListener{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_play, container, false);
        return view;
    }


    @Override
    @OnClick({R.id.text})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text:
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(Constants.URL, "www.baidu.com");
                startActivity(intent);
                break;
        }
    }
}
