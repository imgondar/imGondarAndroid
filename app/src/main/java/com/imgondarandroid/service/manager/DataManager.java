package com.imgondarandroid.service.manager;

import android.content.Context;

import com.imgondarandroid.service.RetrofitHelper;
import com.imgondarandroid.service.RetrofitService;
import com.imgondarandroid.service.entity.UserInfo;

import okhttp3.ResponseBody;
import rx.Observable;

/**
 * Created by wangmuxiong on 2017/12/14.
 */

public class DataManager {

    private RetrofitService mRetrofitService;
    public DataManager(Context context){
        this.mRetrofitService = RetrofitHelper.getInstance(context).getServer();
    }

    public Observable<UserInfo> login(String phone, String password){
        return mRetrofitService.login(phone, password);
    }

    public Observable<ResponseBody> downloadPicFromNet(String url){
        return mRetrofitService.downloadPicFromNet(url);
    }


}
