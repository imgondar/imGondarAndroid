package com.imgondarandroid.service.presenter;

import android.content.Context;
import android.content.Intent;

import com.imgondarandroid.service.manager.DataManager;
import com.imgondarandroid.service.view.SavePictureView;
import com.imgondarandroid.service.view.View;
import com.imgondarandroid.util.BitmapUtil;
import com.imgondarandroid.util.Logger;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by wangmuxiong on 2017/12/15.
 */

public class SavePicturePresenter implements Presenter {
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private SavePictureView savePictureView;

    public SavePicturePresenter (Context mContext){
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        manager = new DataManager(mContext);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        savePictureView = (SavePictureView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    private boolean flag;

    public void savePic(String url, final String path) {
        Logger.i("SavePicturePresenter", "12344");
        mCompositeSubscription.add(manager.downloadPicFromNet(url)
                .subscribeOn(Schedulers.newThread())//在新线程中实现该方法
                .observeOn(AndroidSchedulers.mainThread())//在Android主线程中展示
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onCompleted() {
                        if (flag) {
                            savePictureView.onSuccess(path);
                        }
                    }

                    @Override
                    public void onError(Throwable arg0) {
                        savePictureView.onError("保存失败 " + arg0);
                    }

                    @Override
                    public void onNext(ResponseBody arg0) {
                        flag = BitmapUtil.writeResponseBodyToDisk(path, arg0);
                    }
                }));
    }
}
