package com.imgondarandroid.service.presenter;

import android.content.Intent;

import com.imgondarandroid.service.view.View;

/**
 * Created by wangmuxiong on 2017/12/14.
 */

public interface Presenter {

    void onCreate();

    void onStart();//暂时没用到

    void onStop();

    void pause();//暂时没用到

    void attachView(View view);

    void attachIncomingIntent(Intent intent);//暂时没用到
}
