package com.imgondarandroid.service.presenter;

import android.content.Context;
import android.content.Intent;

import com.imgondarandroid.service.entity.UserInfo;
import com.imgondarandroid.service.manager.DataManager;
import com.imgondarandroid.service.view.UserView;
import com.imgondarandroid.service.view.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by wangmuxiong on 2017/12/14.
 */

public class UserPresenter implements Presenter{

    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private UserView mUserView;
    private UserInfo mUserInfo;
    public UserPresenter (Context mContext){
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        manager = new DataManager(mContext);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mUserView = (UserView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    public void login() {
        mCompositeSubscription.add(manager.login("13643306295", "123456")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserInfo>() {
                    @Override
                    public void onCompleted() {
                        if (mUserInfo != null){
                            mUserView.onSuccess(mUserInfo);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        mUserView.onError("请求失败！！");
                    }

                    @Override
                    public void onNext(UserInfo userInfo) {
                        mUserInfo = userInfo;
                    }
                })
        );

    }
}
