package com.imgondarandroid.service.entity;

import java.io.Serializable;

/**
 * Created by wangmuxiong on 2017/12/14.
 */

public class UserInfo{

    private int code;
    private String msg;
    private String token;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * username : 李四
         * phone : 13643306295
         * avatar : http://botfile.imgondar.com/170908-154559.png
         * large_avatar :
         * birthday : 1995-05-09
         * sex : 男
         * latitude : 39.993049
         * longitude : 116.363494
         * follows_num : 4
         * fans_num : 5
         * honorbills_num : 30
         * driftbills_num : 30
         * age : 22
         */

        private String id;
        private String username;
        private String phone;
        private String avatar;
        private String large_avatar;
        private String birthday;
        private String sex;
        private double latitude;
        private double longitude;
        private int follows_num;
        private int fans_num;
        private int honorbills_num;
        private int driftbills_num;
        private int age;
        private String phone_hide;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getLarge_avatar() {
            return large_avatar;
        }

        public void setLarge_avatar(String large_avatar) {
            this.large_avatar = large_avatar;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public int getFollows_num() {
            return follows_num;
        }

        public void setFollows_num(int follows_num) {
            this.follows_num = follows_num;
        }

        public int getFans_num() {
            return fans_num;
        }

        public void setFans_num(int fans_num) {
            this.fans_num = fans_num;
        }

        public int getHonorbills_num() {
            return honorbills_num;
        }

        public void setHonorbills_num(int honorbills_num) {
            this.honorbills_num = honorbills_num;
        }

        public int getDriftbills_num() {
            return driftbills_num;
        }

        public void setDriftbills_num(int driftbills_num) {
            this.driftbills_num = driftbills_num;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getPhone_hide() {
            return phone_hide;
        }

        public void setPhone_hide(String phone_hide) {
            this.phone_hide = phone_hide;
        }
    }
}
