package com.imgondarandroid.service;

import com.imgondarandroid.service.entity.UserInfo;
import com.imgondarandroid.web.WebUtil;

import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by wangmuxiong on 2017/12/14.
 */

public interface RetrofitService {

    @FormUrlEncoded
    @POST(WebUtil.LOGIN)
    Observable<UserInfo> login(
            @Field("phone") String phone,
            @Field("password") String password
    );

    @GET
    Observable<ResponseBody> downloadPicFromNet(@Url String fileUrl);
}
