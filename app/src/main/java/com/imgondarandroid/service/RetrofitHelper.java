package com.imgondarandroid.service;

import android.content.Context;

import com.google.gson.GsonBuilder;
import com.imgondarandroid.util.Logger;
import com.imgondarandroid.util.TimeUtil;
import com.imgondarandroid.web.WebUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by wangmuxiong on 2017/12/14.
 */

public class RetrofitHelper {

    private Context mCntext;


    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            // 获取参数
            RequestBody requestBody = request.body();
            Buffer buffer = new Buffer();
            requestBody.writeTo(buffer);
            String paramsStr = buffer.readUtf8();
            buffer.close();
            // 拆分参数
            Map<String, String> params = new HashMap<String, String>();
            String[] kvs = paramsStr.split("&");
            for (String kv : kvs) {
                String[] split = kv.split("=");
                if (split.length == 0) continue;
                String k = split[0];
                String v = "";
                if (split.length == 2) {
                    v = split[1];
                }
                Logger.i("PARAMS", "K " + k + " V " + v);
                params.put(k, v);
            }
            params.put("timestamp", String.valueOf(TimeUtil.getCurrentTime()));
            Logger.i("zzz", "request====" + paramsStr);
            // 重新填充参数，加上一些公共参数
            Request.Builder requestBuilder = request.newBuilder();
            if (request.body() instanceof FormBody){
                FormBody.Builder newFormBody = new FormBody.Builder();
                FormBody oldFormBody = (FormBody) request.body();
                for(int i = 0;i<oldFormBody.size();i++){
                    newFormBody.addEncoded(oldFormBody.encodedName(i),oldFormBody.encodedValue(i));
                }
                newFormBody.add("timestamp", String.valueOf(TimeUtil.getCurrentTime()));
                newFormBody.add("sign", WebUtil.getSignature(params));
                requestBuilder.method(request.method(), newFormBody.build());
            }
            Request newRequest = requestBuilder.build();
            okhttp3.Response proceed = chain.proceed(newRequest);
            return proceed;
        }
    }).build();

    GsonConverterFactory factory = GsonConverterFactory.create(new GsonBuilder().create());
    private static RetrofitHelper instance = null;
    private Retrofit mRetrofit = null;

    public static RetrofitHelper getInstance(Context context){
        if (instance == null){
            instance = new RetrofitHelper(context);
        }
        return instance;
    }
    private RetrofitHelper(Context mContext){
        mCntext = mContext;
        init();
    }

    private void init() {
        resetApp();
    }

    private void resetApp() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(WebUtil.BASE_URL)
                .client(client)
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }
    public RetrofitService getServer(){
        return mRetrofit.create(RetrofitService.class);
    }
}
