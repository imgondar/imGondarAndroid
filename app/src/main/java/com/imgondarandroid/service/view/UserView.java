package com.imgondarandroid.service.view;

import com.imgondarandroid.service.entity.UserInfo;

/**
 * Created by wangmuxiong on 2017/12/14.
 */

public interface UserView extends View{
    void onSuccess(UserInfo userInfo);
    void onError(String result);
}
