package com.imgondarandroid.service.view;

/**
 * Created by wangmuxiong on 2017/12/15.
 */

public interface SavePictureView extends View{
    void onSuccess(String path);
    void onError(String result);
}
